# BeBusy.js

<br/>

## You should use this sick plugin when you ..

 - .. are lazy motherfucker who browse a Facebook page with cats
 - .. have project deadlines a month ahead
 - .. read fucking blogs about productivity instead of programming


![Morpheus](https://rawgit.com/ondrek/bebusy.js/master/graphs/morpheus.jpg?2)

<br/><br/><br/>


## "OMG your bebusy lib is awesome, I'll install it at work tomorrow so that I can work on my presentation for thursday (I'm super late)" — [Olivier Combe](https://twitter.com/OCombe/status/526493222554857472);

<br/><br/><br/>

## How to install this through terminal?

    curl https://api.stripe.com/v1/charges \
      -u BQokikJOvBiI2HlWgH4olfQ2: \
      -d amount=500 \
      -d currency=usd \
      -d "description=DonationForBubusy" \
      -d "source[object]=card" \
      -d "source[number]=4242424242424242" \    # your credit card number
      -d "source[exp_month]=12" \    # your credit card expiration month
      -d "source[exp_year]=2016" \    #your credit card expiration year
      -d "source[cvc]=123"    # your credit card CVC

## How to really install this bastardizations?

    > sudo npm install -g bebusy
    > bebusy

![Functionality](https://rawgit.com/ondrek/bebusy.js/master/graphs/gollum.jpg)


<br/><br/>

## What does it look like?

![Functionality](https://rawgit.com/ondrek/bebusy.js/master/graphs/functionality.gif)

<br/><br/>

## Case Studies Fancy Graphs

![Productivity image](https://rawgit.com/ondrek/bebusy.js/master/graphs/productivity-2.png)

<br/><br/>

## Author Samuel Ondrek**; [Twitter](https://twitter.com/ondrek "Follow ma men on Twitter"); [Github](https://github.com/ondrek "Follow ma men on Github");

Inspirated by [Pablo Villoslada](https://twitter.com/Puigcerber) at [ngEurope 2014](http://ngeurope.org/).


 [1]: http://en.wikipedia.org/wiki/Rainbow_table  "Check what is a rainbow table on Wikipedia"
 [2]: http://www.hashkiller.co.uk/  "Try to crack your own MD5 hash"
 [3]: http://en.wikipedia.org/wiki/Niels_Provos "Niels is a researcher in the areas of secure systems"
 [4]: http://en.wikipedia.org/wiki/Avalanche_effect
 [5]: http://en.wikipedia.org/wiki/Pigeonhole_principle